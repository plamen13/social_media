from newsapi import NewsApiClient
import json
import spacy
import sqlite3

threat="flood"
client="Tesco"
nlp=spacy.load('en_core_web_sm')
newsapi=NewsApiClient(api_key='9d39a9e9a3c844e2be984df4d13a6b72')
all_articles = newsapi.get_everything(q=threat + '+'  + client,
                                      sources='bbc-news,the-verge',
                                      domains='bbc.co.uk,techcrunch.com',
                                      from_param='2018-10-25',
                                      to='2018-11-20',
                                      language='en',
                                      sort_by='relevancy')
source=newsapi.get_sources()

"""
format data in a readable format with json
"""
json_text=json.dumps(all_articles, sort_keys=True, indent=4, separators=(':', ','))




conn = sqlite3.connect("fixed1.db")
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS Fire (id INTEGER PRIMARY KEY ,Threat TEXT, Brand TEXT, Location TEXT, Description TEXT, Date DATE, Source TEXT, Link TEXT, Significance real)')
"""
Divide all articles into single articles
for each token in the text check if it has the word 'fire' in it and check if this word is a noun
if the word exist and it is noun: print the title and the content 
use #spacy for processing the word and determine what the word means
"""
location=""
for article in all_articles["articles"]:

    try:
        spacyText = nlp(article["content"])
    except:
        print("")
    for token in spacyText:
        if token.lemma_ == 'flood' and str(token) != 'flooded with' and str(token) != 'flooded of' and \
                str(token) != 'flooded with' and str(token) != 'flood it' and str(token) != 'flood of' and \
                str(token) != 'flood with,' and str(token) != 'Flooded with' and str(token) != 'Flooding back' and \
                str(token) != 'Floods of tears' and str(token) != 'Immigrant' and str(token) != 'Market' and \
                str(token) != 'Migrant' and str(token) != 'Migration' and str(token) != 'Tears':
            #print(article["title"])
            #print(article["content"])
            for ent in spacyText.ents:
                toBeWritten=(ent.text,  ent.label_)

                if ent.label_ == "FAC":
                    building = ent.text
                if ent.label_ == "ORG":
                    company = ent.text
                if ent.label_ == "GPE":
                    location = ent.text
                if ent.label_ == "LOC":
                    loc = ent.text
                    loc = loc + "," + loc
                if ent.label_ == "EVENT":
                    event = ent.text
                if ent.label_ == "LANGUAGE":
                    language = ent.text
                if ent.label_ == "DATE":
                    date = ent.text
                if ent.label_ == "TIME":
                    time = ent.text
                if ent.label_ == "MONEY":
                    money = ent.text
                if ent.label_ == "QUANTITY":
                    quantity = ent.text
                    #print(toBeWritten)
                print(toBeWritten)
            c.execute("INSERT INTO Fire (Threat,  Brand, Location, Description, Date, Source, Link, Significance)"
                          "VALUES(?,?,?,?,?,?,?,?)",
                          (threat, client, location, article['description'], article['publishedAt'],
                           str(article['source']), article['url'], 1))
            conn.commit()
            break


def retrieve_from_db():
    c.execute('SELECT * FROM Fire')
    for row in c.fetchall():
        print(row)

retrieve_from_db()


c.close()
conn.close()