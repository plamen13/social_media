from newsapi import NewsApiClient
import json
import spacy
import sqlite3

filter_exclude_list=['flooded'+'with', 'flooded'+'of', 'flooded'+'with', 'flood'+'it', 'flood'+'of','flood' + 'with', 'Flooded'+'with',
            'Flooding'+'back', 'Floods'+ 'of' + 'tears','Immigrant', 'Migrant', 'Migration', 'Market', 'Tears']


client=["Tesco", "Asda", "Aldi", "sainsbury's",]
threat=["flood", "fire", "hurricane"]
nlp=spacy.load('en_core_web_sm')
location = ""
conn = sqlite3.connect("fixed10.db")
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS Fire (id INTEGER PRIMARY KEY ,Threat TEXT, Brand TEXT, Location TEXT, Description TEXT, Date DATE, Source TEXT, Link TEXT, Significance real)')
"""
get all the dates from the database before execution
"""
"""
retrieve all dates from the db so to check if an article exists in the db  
"""
existing_dates=c.execute('SELECT Date FROM Fire').fetchall()
for i in client:
    for b in threat:
        newsapi=NewsApiClient(api_key='9d39a9e9a3c844e2be984df4d13a6b72')
        all_articles = newsapi.get_everything(q=i + "+" + b,
                                      sources='bbc-news,the-guardian-uk, daily-mail, the-telegraph, metro, google-news-uk',
                                      domains='bbc.co.uk, theguardian.com/uk, dailymail.co.uk, telegraph.co.uk, '
                                              'metro.co.uk, news.google.com/?hl=en-GB&gl=GB&ceid=GB:en  ',
                                      from_param='2018-10-29',
                                      to='2018-11-20',
                                      language='en',
                                      sort_by='relevancy')
        source=newsapi.get_sources()
        """
        format data in a readable format with json
        """

        json_text = json.dumps(all_articles, sort_keys=True, indent=4, separators=(':', ','))
        """
        Divide all articles into single articles
        use list of filters to check if the words needs to be excluded
        use #spacy for processing the word and determine what the word means
        use #spacy named entities to determine what everyone of them means and get them ready for db imput
        input results into db
        """

        for article in all_articles["articles"]:
            spacyText = nlp(article["content"])
            #new_dates.append(new_dates)
            """
            check if the article's date exist in the db
            """
            for token in spacyText:
                word=[]
                word.append(str(token))
                date = article['publishedAt']

                    # print(article["title"])
                    # print(article["content"])
                for ent in spacyText.ents:
                    toBeWritten = (ent.text, ent.label_)
                    if ent.label_ == "FAC":
                        building = ent.text
                    if ent.label_ == "ORG":
                        company = ent.text
                    if ent.label_ == "GPE":
                        location = ent.text
                    if ent.label_ == "LOC":
                        loc = ent.text
                        loc = loc + "," + loc
                    if ent.label_ == "EVENT":
                        event = ent.text
                    if ent.label_ == "LANGUAGE":
                        language = ent.text
                    if ent.label_ == "DATE":
                        date1 = ent.text
                    if ent.label_ == "TIME":
                        time = ent.text
                    if ent.label_ == "MONEY":
                        money = ent.text
                    if ent.label_ == "QUANTITY":
                        quantity = ent.text
                            # print(toBeWritten)
                            #print(toBeWritten)
                        """
                        check if any ot items in the word list(all the e)
                        """
            if any(i in word for i in filter_exclude_list) or str(date) in str(existing_dates):
                break
            c.execute(
                        "INSERT INTO Fire (Threat,  Brand, Location, Description, Date, Source, Link, Significance)"
                        "VALUES(?,?,?,?,?,?,?,?)",
                        (b, i, location, article['description'], article['publishedAt'],
                         str(article['source']), article['url'], 1))

            break
        conn.commit()

        #print(json_text)



def retrieve_from_db():
    c.execute('SELECT * FROM Fire')
    for row in c.fetchall():
        print(row)

retrieve_from_db()


c.close()
conn.close()