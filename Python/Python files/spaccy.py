import spacy
nlp=spacy.load('en_core_web_sm')

text = ("the hurricane caused immediate flooding and severe damage along the shore."
          "The winds and floods aren't just threatening Florida either - southern Alabama and Georgia residents"
          "were also warned about the potential damage from the storm. The storm wreaked havoc in the Florida Panhandle,"
          "damaging hospitals and homes, cutting off transportation, and leaving over a million buildings without electricity. ")

doc=nlp(text)

for entity in doc.ents:
    #print (entity.text, entity.label_)
    print(entity.text, entity.label_)

#doc1= nlp('my fries were super gross')
#doc2=nlp('such disgusting fries')
#similiarity = doc1.similiarity(doc2)
#print (doc1.text, doc2.text, similiarity)
