import spacy
help(spacy)
nlp=spacy.load('en_core_web_sm')

text = ("When Sebastian Thrun started working on self-driving cars at "
        u"Google in 2007, few people outside of the company took him "
        u"seriously. “I can tell you very senior CEOs of major American "
        u"car companies would shake my hand and turn away because I wasn’t "
        u"worth talking to,” said Thrun, now the co-founder and CEO of "
        u"online higher education startup Udacity, in an interview with "
        u"Recode earlier this week.")

doc=nlp(text)

for entity in doc.ents:
    print (entity.text, entity.label_)

doc1= nlp('my fries were super gross')
doc2=nlp('such disgusting fries')
similiarity = doc1.similiarity(doc2)
print (doc1.text, doc2.text, similiarity)
