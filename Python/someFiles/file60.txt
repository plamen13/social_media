{
    "articles":[
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright Getty Images It's the final call, say scientists, the most extensive warning yet on the risks of rising global temperatures. Their dramatic report on keeping that rise under 1.5 degrees C states that the world is now completely off track, head\u2026 [+7862 chars]",
            "description":"A new study urges rapid and fundamental changes \"in all aspects of society\" to limit climate warming.",
            "publishedAt":"2018-10-08T01:00:12Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Climate report: Scientists politely urge 'Act now, idiots'",
            "url":"https://www.bbc.co.uk/news/science-environment-45775309",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/FF5F/production/_103757356_ip5.png"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright Getty Images Image caption The Uttar Pradesh government has said that it conducts \"police encounters\" to improve law and order A series of alleged extra-judicial killings by police in the northern Indian state of Uttar Pradesh has raised serio\u2026 [+7921 chars]",
            "description":"Two fatal shooting incidents have raised concerns about \"trigger-happy cops\" in India's Uttar Pradesh state.",
            "publishedAt":"2018-10-04T00:29:17Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"The Indian state where police kill with impunity",
            "url":"https://www.bbc.co.uk/news/world-asia-india-45719940",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/CF28/production/_98123035_gettyimages-73254912.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright AFP Image caption Shimaa Qassem, who won the Miss Iraq contest in 2015, has 2.7 million Instagram followers A former Miss Iraq says she has received death threats, days after another Iraqi model was shot dead. Shimaa Qassem voiced fears for he\u2026 [+2598 chars]",
            "description":"Shimaa Qassem says she fears for her life after the murder in Baghdad of another Iraqi Instagram star.",
            "publishedAt":"2018-10-01T12:40:43Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Former Miss Iraq 'threatened' after fellow Instagram star's murder",
            "url":"https://www.bbc.co.uk/news/world-middle-east-45706227",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/F02C/production/_103648416_gettyimages-501996802.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Charlie Sloth is leaving BBC Radio 1 and 1Xtra after 10 years. He currently presents The 8th and The Rap Show for the networks and previously hosted 1Xtra's 4-7pm show. His hugely popular Fire In The Booth has seen appearances from Stormzy, Bugzy Malone, Drak\u2026 [+1216 chars]",
            "description":"The popular host of Fire in the Booth will present his last show in November",
            "publishedAt":"2018-10-03T21:09:55Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Charlie Sloth to leave BBC Radio 1 & 1Xtra after nearly 10 years",
            "url":"https://www.bbc.co.uk/news/newsbeat-45741140",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/169FB/production/_102576629_charliedrake2.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright Simon Moores Image caption A drone picture taken on 15 September shows the extent of the fire A fire at a warehouse in Westwood near Margate has finally been extinguished 25 days after it started, Kent Fire and Rescue has said. At its height a\u2026 [+869 chars]",
            "description":"Four children were arrested on suspicion of arson after the blaze broke out on 15 September.",
            "publishedAt":"2018-10-10T09:09:37Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Margate warehouse fire put out after 25 days",
            "url":"https://www.bbc.co.uk/news/uk-england-kent-45808542",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/1189B/production/_103453817_fire.jpg"
        },
        {
            "author":null,
            "content":"Jordan Sancho is the first player born this century to be called up to the England senior squad Jadon Sancho marked his first England senior call-up with another assist as Bundesliga leaders Borussia Dortmund beat Augsburg in a thriller. Lucien Favre's side t\u2026 [+1239 chars]",
            "description":"England's Jadon Sancho claims sixth assist as Borussia Dortmund beat Augsburg in a thriller in the Bundesliga.",
            "publishedAt":"2018-10-06T19:32:27Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Borussia Dortmund 4-3 Augsburg: England's Sancho assist",
            "url":"https://www.bbc.co.uk/sport/football/45772689",
            "urlToImage":"https://ichef.bbci.co.uk/onesport/cps/624/cpsprodpb/171F1/production/_103750749_sancho_reuters.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image caption Residents say the straw bales are an eyesore and a fire risk Residents furious over a \"monstrous\" wall of straw bales at the bottom of their gardens have been told they are not a safety risk. The structure, more than 30ft (9.1m) tall, was erecte\u2026 [+1659 chars]",
            "description":"The Health and Safety Executive said the bales in Ockbrook were \"stacked satisfactorily\".",
            "publishedAt":"2018-10-05T18:43:11Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"'No risk' over Derbyshire farm's huge straw bale wall",
            "url":"https://www.bbc.co.uk/news/uk-england-derbyshire-45764122",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/EB40/production/_103042206_mediaitem103042205.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright PA A couple who fled Grenfell Tower told their children they were going on a \"night-time adventure\" as they escaped. In a written statement to the inquiry into the blaze, Jamie Murray and Mahad Egal said they were woken up by frantic knocking \u2026 [+3833 chars]",
            "description":"Parents who fled Grenfell Tower told the inquiry they played a game to stop their children panicking.",
            "publishedAt":"2018-10-03T19:33:21Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Grenfell Tower inquiry: Children told escape was an 'adventure'",
            "url":"https://www.bbc.co.uk/news/uk-45739427",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/770E/production/_103687403_5ef17c0e-c653-4beb-8c2b-53a904bdf2ce.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image caption Thirteen people were killed on Bloody Sunday in January 1972 and another died of his injuries some months later The families of two men shot dead on Bloody Sunday have been awarded a combined \u00a3700,000 in compensation. The family of Gerard McKinn\u2026 [+1201 chars]",
            "description":"Gerard McKinney's family are given \u00a3625,000 while Michael McDaid's receive \u00a375,000.",
            "publishedAt":"2018-10-01T14:05:57Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Bloody Sunday: Families of two men shot dead awarded \u00a3700k",
            "url":"https://www.bbc.co.uk/news/uk-northern-ireland-foyle-west-45710550",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/F85E/production/_96028536_bloodyinjured.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright Getty Images Image caption Russian President Vladimir Putin (left) and Indian PM Narendra Modi signed the deal in Delhi on Friday India has signed a deal with Russia to acquire the S-400 air defence missile system, despite the possibility such\u2026 [+6864 chars]",
            "description":"India's purchase of the Russian-made S-400 air defence system runs the risk of US sanctions. So why buy it?",
            "publishedAt":"2018-10-05T12:57:36Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"S-400: India missile defence purchase in US-Russia crosshairs",
            "url":"https://www.bbc.co.uk/news/world-asia-india-45757556",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/9198/production/_103727273_c277ce01-cf10-4984-b5b0-92322020a50f.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Media caption Ford has agreed to refund hundreds of drivers after they suffered engine problems Ford has offered to refund thousands of pounds to customers whose engines have failed, following a BBC investigation. Hundreds of customers have said their cars wi\u2026 [+5760 chars]",
            "description":"Drivers have reported cars with EcoBoost engines overheating and failing or even bursting into flames.",
            "publishedAt":"2018-09-30T23:19:23Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Ford to refund engine fail customers",
            "url":"https://www.bbc.co.uk/news/uk-england-45628325",
            "urlToImage":"https://ichef.bbci.co.uk/images/ic/1024x576/p06mg67s.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image caption The two men were found dead at Banham Poultry in the early hours Two men have died in an industrial accident at a poultry firm in Norfolk. Police were called to Banham Poultry, Station Road, Attleborough at about 01:10 BST following reports two \u2026 [+953 chars]",
            "description":"The pest control workers were killed in a suspected chemical incident.",
            "publishedAt":"2018-10-04T09:02:36Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Two dead in accident at Banham Poultry in Norfolk",
            "url":"https://www.bbc.co.uk/news/uk-england-norfolk-45745192",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/CE22/production/_103707725_img_3331.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright BBC/Getty Images Image caption Sir Lenny has admitted to \"channelling Pryor\" in his early years in comedy Sir Lenny Henry is to play US comedian Richard Pryor in a one-man play he has written himself. Richard Pryor on Fire will be staged at th\u2026 [+1638 chars]",
            "description":"One comedian will play tribute to another in a one-man play about the confrontational stand-up star.",
            "publishedAt":"2018-10-03T13:07:08Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Sir Lenny Henry to play Richard Pryor in one-man play",
            "url":"https://www.bbc.co.uk/news/entertainment-arts-45732348",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/8B2C/production/_103682653_henry1_bbcgetty.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Media caption A train caught fire at Grantham on Friday Train used on a service between Hull and London are \"not fit for purpose\", an East Yorkshire MP has said. Hull Trains is not running a direct service between Hull and London after a fire onboard a train \u2026 [+1511 chars]",
            "description":"Hull Trains is not running a direct service between Hull and London after a fire onboard a train.",
            "publishedAt":"2018-10-08T08:26:22Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Hull Trains services 'not fit for purpose', says MP",
            "url":"https://www.bbc.co.uk/news/uk-england-humber-45782239",
            "urlToImage":"https://ichef.bbci.co.uk/images/ic/1024x576/p06n84cb.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"A fire appliance has been involved in a collision at a busy junction in Glasgow. The crash took place at about 08:00, at the junction of Govan Road and Golspie Street. A white car has been badly damaged in the collision and the fire service vehicle has come t\u2026 [+102 chars]",
            "description":"The fire service vehicle and a car are badly damaged in the rush-hour collision in Govan.",
            "publishedAt":"2018-10-03T07:27:21Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Fire appliance crashes at busy Glasgow junction",
            "url":"https://www.bbc.co.uk/news/uk-scotland-glasgow-west-45729474",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/FD32/production/_103681846_mediaitem103681845.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"No tax rises on petrol, says May Keeping the freeze will cost the government \u00a338bn over three years, the chancellor has said previously.",
            "description":"The latest on the controversy surrounding plans for the Scottish League Cup semi-finals make the front pages.",
            "publishedAt":"2018-10-03T05:41:20Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"The papers: Football chiefs under fire",
            "url":"https://www.bbc.co.uk/news/uk-scotland-45729201",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/B67E/production/_103681764_sun.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright PA Three teenagers have been charged in connection with a fire at an indoor market in Dundee last month. More than 60 firefighters tackled the blaze at the building in Main Street on Wednesday 12 September. Police said two boys, aged 13 and 15\u2026 [+220 chars]",
            "description":"More than 60 firefighters tackled the blaze at the indoor market in Dundee last month.",
            "publishedAt":"2018-10-09T20:41:57Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Teenagers charged over Dundee indoor market fire",
            "url":"https://www.bbc.co.uk/news/uk-scotland-tayside-central-45805655",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/129F7/production/_103397267_hi049242064.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image caption Smells have been reported in parts of London including Bermondsey There have been reports of an \"acrid, noxious and strange chemical smell\" across parts of London. The odour appears to be affecting areas stretching from Victoria to Wapping in ea\u2026 [+1297 chars]",
            "description":"The \"lingering\" smell appears to be affecting central, eastern and south-eastern areas of the city.",
            "publishedAt":"2018-10-10T21:03:31Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"London smell: Reports of 'acrid, noxious' odour across the city",
            "url":"https://www.bbc.co.uk/news/uk-england-london-45818317",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/A1E0/production/_103804414_hi026144103.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"Image copyright PA Image caption \u00a350m will be used to help support people's physical and mental health needs following the tragedy Grenfell survivors will be given special screenings to examine the effects of smoke inhalation and asbestos exposure, health chi\u2026 [+1522 chars]",
            "description":"NHS England said it would provide up to \u00a350m to fund long-term screening and treatment for those affected by the Grenfell Tower fire.",
            "publishedAt":"2018-10-10T10:55:45Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"Grenfell Tower survivors to get smoke damage screening",
            "url":"https://www.bbc.co.uk/news/uk-england-london-45811419",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/DE07/production/_103793865_hi049727592.jpg"
        },
        {
            "author":"https://www.facebook.com/bbcnews",
            "content":"At least 50 people have died after an oil tanker collided with a car on a major road in the west of the Democratic Republic of the Congo. The incident occurred near the city of Kisantu, between the capital Kinshasa and the port of Matadi. One hundred people s\u2026 [+543 chars]",
            "description":"Dozens of people suffered second-degree burns after a tanker collided with a car in the DR Congo.",
            "publishedAt":"2018-10-06T13:59:59Z",
            "source":{
                "id":"bbc-news",
                "name":"BBC News"
            },
            "title":"At least 50 dead after oil tanker collision in DR Congo",
            "url":"https://www.bbc.co.uk/news/world-africa-45772028",
            "urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_news/10CB0/production/_103748786_kisantukinshasamatadidrcongo102018.png"
        }
    ],
    "status":"ok",
    "totalResults":108
}