from newsapi import NewsApiClient
import json
import spacy


nlp=spacy.load('en_core_web_sm')
newsapi=NewsApiClient(api_key='9d39a9e9a3c844e2be984df4d13a6b72')
all_articles = newsapi.get_everything(q='flood''+flood',
                                      sources='bbc-news,the-verge',
                                      domains='bbc.co.uk,techcrunch.com',
                                      from_param='2018-10-25',
                                      to='2018-11-20',
                                      language='en',
                                      sort_by='relevancy')
source=newsapi.get_sources()

"""
format data in a readable format with json
"""
# print(all_articles)
json_text=json.dumps(all_articles, sort_keys=True, indent=4, separators=(':', ','))
print(json_text)


#spacy
#spacyText = nlp(json_text)
"""
Divide all articles into single articles
for each token in the text check if it has the word 'fire' in it and check if this word is a noun
if the word exist and it is noun: print the title and the content 
use #spacy for processing the word and determine what the word means
"""
for article in all_articles["articles"]:
    spacyText = nlp(article["content"])
    for token in spacyText:
        if token.lemma_ == "flood": #and token.pos_ == "NOUN":
            print(article["title"])
            print(article["content"])
            for ent in spacyText.ents:
                toBeWritten=(ent.text,  ent.label_)
                print(toBeWritten)

            break


