from newsapi import NewsApiClient
import json
import spacy
import sqlite3
import cerberus
from cerberus import Validator
import datetime


newsapi = NewsApiClient(api_key='9d39a9e9a3c844e2be984df4d13a6b72')

client = 'Tesco' or 'tesco'
threat = 'flood'
available_articles = newsapi.get_everything(q='fire+' + client,
                                            sources='bbc-news,the-verge',
                                            domains='bbc.co.uk,techcrunch.com',
                                            from_param='2018-10-25',
                                            to='2018-11-20',
                                            language='en',
                                            sort_by='publishedAt', )
source = newsapi.get_sources()

"""
Create database
"""
conn = sqlite3.connect("Trial")
c = conn.cursor()


"""
format data in a readable format with json
"""
json_text = json.dumps(available_articles, sort_keys=True, indent=4, separators=(':', ','))
nlp = spacy.load('en_core_web_sm')
spacy_text = nlp(json_text)


"""
Create a schema using the "spacy" types; define a validator 
"""



"""
Divide all articles into single articles
for each token in the text check if it has the word 'fire' in it and check if this word is a noun
if the word exist and it is noun: print the title and the content 
use #spacy for processing the word and determine what the word means
print "spacy" output; validate "spacy" output; 
"""

"""
       if 'flooded with' and 'flooded of' and 'flooded with' and 'flood it' and 'flood of' and 'flood with,' and 'Flooded with' \
       and 'Flooding back' and 'Floods of tears' and 'Immigrant' and 'Market' and 'Migrant' and 'Migration' and 'Tears' \
           not in article:
           """


def exclude_mismatch_analyze_text():
    global spacy_text
    global analyzed_document
    for article in available_articles["articles"]:
        spacyText = nlp(article["content"])
        for token in spacyText:
            if 'flooded with' and 'flooded of' and 'flooded with' and 'flood it' and 'flood of' and 'flood with,' and 'Flooded with' \
                    and 'Flooding back' and 'Floods of tears' and 'Immigrant' and 'Market' and 'Migrant' and 'Migration' and 'Tears' \
                    not in article:
                title = article['title']
                information =article['description']
                timePublished =article['publishedAt']
                media = article['source']
                link = article['url']
                for ent in spacyText.ents:
                    analyzed_document = (ent.label_, ent.text)
                    #print(title)
                    #print(information)
                    #print(timePublished)
                    #print(media)
                    #print(link)
                    #print(analyzed_document)
                    return analyzed_document
                break


def validate_named_entities():
    global valid
    schema = {
        'PERSON':{'type':'dict'},
        'FAC': {'type': 'string'},
        'ORG': {'type': 'string'},
        'GPE': {'type': 'string'},
        'LOC:': {'type': 'string'},
        'EVENT': {'type': 'string'},
        'LANGUAGE': {'type': 'string'},
        'DATE': {'type': ['date']},
        'TIME': {'type': 'datetime'},
        'MONEY': {'type': ['number', ]},
        'QUANTITY': {'type': 'number'},
        'CARDINAL': {'type': ['number', 'string', 'float']}}
    v = Validator(schema)
    #analyzed_document=exclude_mismatch_analyze_text()
    #dict_to_be_analyzed=literal_eval(analyzed_document)
    valid=""
    try:
        valid = v.validate(analyzed_document)
    except:
        print("hi")
    return valid


def prepare_validated_entities_for_db_input():
    information = exclude_mismatch_analyze_text()
    timePublished = exclude_mismatch_analyze_text()
    media = exclude_mismatch_analyze_text()
    link = exclude_mismatch_analyze_text()
    valid=validate_named_entities()
    print(valid)
    if valid:
        for ent in spacy_text.ents:
            if ent.label_ == "FAC":
                building = ent.text
            if ent.label_ == "ORG":
                company = ent.text
            if ent.label_ == "GPE":
                location = ent.text
                location = location + "," + location
            if ent.label_ == "LOC":
                loc = ent.text
                loc = loc + "," + loc
            if location == "" or loc == "":
                location = "No mentioned location "
            if ent.label_ == "EVENT":
                event = ent.text
            if ent.label_ == "LANGUAGE":
                language = ent.text
            if ent.label_ == "DATE":
                date = ent.text
            if ent.label_ == "TIME":
                time = ent.text
            if ent.label_ == "MONEY":
                money = ent.text
            if ent.label_ == "QUANTITY":
                quantity = ent.text
        c.execute("INSERT INTO Fire (Threat, Name, Brand, Location, Description, Date, Source, Link, Significance)"
                      "VALUES(?,?,?,?,?,?,?,?,?)",
                        (threat, client, '', str(location), information, timePublished, str(media), link, 1,))
conn.commit()


        # print(spacy.explain('GPE'))
        # print(v.validate(analyzed_document))
        # print(v.errors)


def retrieve_from_db():
    c.execute('SELECT * FROM Trial')
    for row in c.fetchall():
        print(row)
    c.close()
    conn.close()


exclude_mismatch_analyze_text()
validate_named_entities()
prepare_validated_entities_for_db_input()
retrieve_from_db()

